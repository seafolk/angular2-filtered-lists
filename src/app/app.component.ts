import { Component, ViewChild, ElementRef, OnInit, Pipe, PipeTransform } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent implements OnInit {
  @ViewChild('input')
  input: ElementRef;
  infoItem: {name:"", flags: any[]};
  sortBy = false;
  filterBy = "folder";

  itemsLeft = [];
  itemsRight = [];

  constructor() {
    this.infoItem = {name:"", flags: []};
    
    for(let i=0; i<100; i++){
      this.itemsLeft.push({
        name: 'item ' + i,
        flags: this._flagsRand()
      })
    }

   for(let i=0; i<100; i++){
      this.itemsRight.push({
        name: 'item ' + i,
        flags: this._flagsRand()
      })
    }
  }

  _flagsRand () {
    let flags = ['home','folder','grade','help']; // https://material.io/icons/
    return flags;
  }

  /**
   * Выводит информацию по текущему выбранному элементу любого списка.
   * При этом выбранный элемент должен выделяться визуально в самом списке.
   */
  onSelect(item){
    this.infoItem = item;
  }

  onFilterRigthList(filterBy: string){
    this.filterBy = filterBy;
  }

  ngOnInit(){
      let eventObservable = Observable.fromEvent(this.input.nativeElement, 'keyup')
      eventObservable.subscribe();
    }
}
/*
 * TODO
 * При этом выбранный элемент должен выделяться визуально в самом списке.
 * Drag-n-drop элементов между списками.
 */

@Pipe({
  name: 'searchPipe',
  pure: false
})
export class SearchPipe implements PipeTransform {
  transform(data: any[], searchTerm: string, orderBy: boolean): any[] {
      searchTerm = searchTerm.toUpperCase();
      return data.filter(item => {
        return item.name.toUpperCase().indexOf(searchTerm) !== -1;
      })
      .sort((a:any, b:any) => {
          let st = 0;
          
          if(orderBy && a.name < b.name)
            st = -1;
          else
            st = 1;

          return st;
      });
  }
}


/**
 * Сверху чекбокс-фильтры, фильтрующие список по наличию свойств у элемента.
 */
@Pipe({
  name: 'filterRigthListPipe',
  pure: false
})
export class FilterRigthListPipe implements PipeTransform {
  transform(data: any[], filterBy: string): any[] {
      return data.filter(item => {
        return item.flags.includes(filterBy);
      });
  }
}